# Generated by Django 3.2.6 on 2021-08-07 12:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_positioncategory_car'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='credit',
            name='model',
        ),
        migrations.AddField(
            model_name='car',
            name='model',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='car',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cridets', to='app.car'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='credit',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='payments', to='app.credit'),
        ),
        migrations.DeleteModel(
            name='PositionCategory',
        ),
    ]
