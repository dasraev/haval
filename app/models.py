from django.db import models

class Car(models.Model):
    name=models.TextField()
    def __str__(self):
        return f'{self.name}'
class  PositionCategory(models.Model):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,null=True)
    position = models.TextField()
    def __str__(self):
        return f'{self.car} | {self.position}'


class Credit(models.Model):
    CHOICE = (
        ("1", ('Credit')),
        ("2", ('Leasing')),
    )
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='cridets',null=True)
    model = models.ForeignKey(PositionCategory,on_delete=models.CASCADE,related_name='cridets',null=True)
    month = models.IntegerField(default=6)
    status = models.CharField(max_length=1, choices=CHOICE)

    def __str__(self):
        return f' {self.model}  |{self.month} '


class Payment(models.Model):
    credit = models.ForeignKey(Credit, on_delete=models.CASCADE, related_name='payments',null=True)
    order = models.IntegerField(default=1)
    sum = models.FloatField()
    percent = models.FloatField()
    total = models.FloatField()
    remain = models.FloatField()

    def __str__(self):
        return f'{self.credit} | {self.total}'




