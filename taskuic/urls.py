from django.contrib import admin
from django.urls import path
from app.views import *
from rest_framework.response import Response
from django.shortcuts import render
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',CreditPaymentView.as_view())
]
